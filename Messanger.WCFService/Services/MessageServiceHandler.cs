﻿using Messanger.Persistance.Entities.Chat;
using Messanger.Protocol;
using Messanger.Protocol.Handlers;
using Messanger.Server.Controllers.V1;
using System;


namespace Messanger.WCFService.Services
{
    public class MessageServiceHandler : IMessageServiceHandler
    {
        private readonly MessageController _messageController;

        public MessageServiceHandler()
        {
            _messageController = Program.Application.ResolveController<MessageController>();
        }

        public string Get(string id) =>
            _messageController?.Get(id);

        public string Create(Message entity) =>
            _messageController?.Create(entity);

        public string Update(Message entity) =>
            _messageController?.Update(entity);

        public string Delete(string id ) =>
            _messageController?.Delete(id);
    }
}
