﻿using Messanger.Persistance.Entities;
using Messanger.Protocol;
using Messanger.Protocol.Handlers;
using Messanger.Server.Controllers.V1;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.WCFService.Services
{
    public class ApplicationUserServiceHandler : IApplicationUserServiceHandler
    {
        private readonly ApplicationUserController _applicationUser;

        public ApplicationUserServiceHandler()
        {
            _applicationUser = Program.Application.ResolveController<ApplicationUserController>();
        }

        public string Get(string id) =>
            _applicationUser?.Get(id);

        public string Create(ApplicationUser entity) => null;
        //_applicationUser?.Create(entity);

        public string Update(ApplicationUser entity) => null;
        //_applicationUser?.Update(entity);

        public string Delete(string id) =>
            _applicationUser?.Delete(id);
    }
}
