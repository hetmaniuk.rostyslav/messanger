﻿using Messanger.Persistance.Entities.Chat;
using Messanger.Protocol;
using Messanger.Protocol.Handlers;
using Messanger.Server.Controllers.V1;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.WCFService.Services
{
    public class RoomServiceHandler : IRoomServiceHandler
    {
        private readonly RoomController _roomController;

        public RoomServiceHandler()
        {
            _roomController = Program.Application.ResolveController<RoomController>();        
        }

        public string Get(string id) =>
            _roomController?.Get(id);

        public string GetMany() =>
            _roomController?.GetMany();

        public string Create(Room entity) =>
            _roomController?.Create(entity);

        public string Update(Room entity) =>
            _roomController?.Update(entity);

        public string Delete(string id) =>
            _roomController?.Delete(id);
    }
}
