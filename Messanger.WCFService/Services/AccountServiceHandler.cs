﻿using Messanger.Persistance.Entities;
using Messanger.Persistance.Models;
using Messanger.Protocol;
using Messanger.Protocol.Handlers;
using Messanger.Server;
using Messanger.Server.Controllers.V1;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.WCFService.Services
{
    public class AccountServiceHandler : IAccountServiceHandler
    {
        private readonly AccountController _accountController;

        public AccountServiceHandler()
        {
            _accountController = Program.Application.ResolveController<AccountController>();
        }

        public string Login(LoginModel model) =>
            _accountController?.Login(model);

        public string Register(RegisterModel model) =>
            _accountController?.Register(model);
    }
}
