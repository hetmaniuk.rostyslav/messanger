﻿using JKang.IpcServiceFramework;
using Messanger.Protocol;
using Messanger.Protocol.Handlers;
using Messanger.Server;
using Messanger.WCFService.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Messanger.WCFService
{
    public class Program
    {
        public static ApplicationStarter Application { get; set; }

        public static void Main(string[] args)
        {
            Console.WriteLine("Initialization DI container!");
            Console.WriteLine();

            IServiceCollection services = ConfigureServices(new ServiceCollection());

            Console.WriteLine("DI container successfully configured!");
            Console.WriteLine();

            IIpcServiceHost host = SetupListeners(services);

            var source = new CancellationTokenSource();

            Task.WaitAll(host.RunAsync(source.Token), Task.Run(() =>
            {
                Console.WriteLine("WCF service started!");

                Application = ApplicationStarter.GetInstance();

                Console.WriteLine();

                Console.WriteLine("Press any key to shutdown.");

                Console.ReadKey();

                Application.Stop();
                source.Cancel();
            }));

            Console.WriteLine("Server stopped.");
        }

        private static IIpcServiceHost SetupListeners(IServiceCollection services)
        {
            var host = new IpcServiceHostBuilder(services.BuildServiceProvider())
                            .AddNamedPipeEndpoint<IMessageServiceHandler>(name: "messageEndpoint", pipeName: "message")
                            .AddNamedPipeEndpoint<IRoomServiceHandler>(name: "roomEndpoint", pipeName: "room")
                            .AddNamedPipeEndpoint<IApplicationUserServiceHandler>(name: "applicationUserEndpoint", pipeName: "applicationUser")
                            .AddNamedPipeEndpoint<IAccountServiceHandler>(name: "accountEndpoint", pipeName: "account")
                            .Build();
            return host;
        }

        private static IServiceCollection ConfigureServices(IServiceCollection services)
        {
            return services
                .AddLogging(builder =>
                {
                    builder.SetMinimumLevel(LogLevel.Trace);
                })
                .AddIpc(builder =>
                {
                    builder
                        .AddNamedPipe(options =>
                        {
                            options.ThreadCount = 2;
                        })
                        .AddService<IAccountServiceHandler, AccountServiceHandler>()
                        .AddService<IApplicationUserServiceHandler, ApplicationUserServiceHandler>()
                        .AddService<IRoomServiceHandler, RoomServiceHandler>()
                        .AddService<IMessageServiceHandler, MessageServiceHandler>();
                });
        }
    }
}
