﻿using Messanger.Persistance.Entities;
using Messanger.Persistance.Repos.Interfaces;
using Messanger.Services.Interfaces;

namespace Messanger.Services
{
    public class ApplicationUserService : IApplicationUserService
    {
        private readonly IApplicationUserRepository _applicationUserRepository;

        public ApplicationUserService(IApplicationUserRepository applicationUserRepository)
        {
            _applicationUserRepository = applicationUserRepository;
        }

        public ApplicationUser Get(string id)
        {
            return _applicationUserRepository.FindOne(x => x.Id == id);
        }

        public ApplicationUser GetByEmail(string email)
        {
            return _applicationUserRepository.FindOne(x => x.Email == email);
        }

        public ApplicationUser Create(ApplicationUser entity)
        {
            return _applicationUserRepository.InsertOne(entity);
        }

        public ApplicationUser Update(ApplicationUser entity)
        {
            return _applicationUserRepository.UpdateOne(entity);
        }

        public ApplicationUser Delete(string id)
        {
            return _applicationUserRepository.DeleteOne(x => x.Id == id);
        }
    }
}
