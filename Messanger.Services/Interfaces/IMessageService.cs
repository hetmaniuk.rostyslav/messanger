﻿using Messanger.Persistance.Entities.Chat;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Services.Interfaces
{
    public interface IMessageService
    {
        Message Get(string id);
        Message Create(Message entity);
        Message Update(Message entity);
        Message Delete(string id);
    }
}
