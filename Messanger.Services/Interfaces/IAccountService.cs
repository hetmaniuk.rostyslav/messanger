﻿using Messanger.Persistance.Entities;
using Messanger.Persistance.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Services.Interfaces
{
    public interface IAccountService
    {
        ApplicationUser Login(LoginModel loginModel);
        bool Register(RegisterModel loginModel);
    }
}
