﻿using Messanger.Persistance.Entities.Chat;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Services.Interfaces
{
    public interface IRoomService
    {
        Room Get(string id);
        IEnumerable<Room> GetMany();
        Room Create(Room entity);
        Room Update(Room entity);
        Room Delete(string id);
    }
}
