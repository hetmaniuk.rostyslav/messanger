﻿using Messanger.Persistance.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Services.Interfaces
{
    public interface IApplicationUserService
    {
        ApplicationUser Get(string id);
        ApplicationUser GetByEmail(string id);
        ApplicationUser Create(ApplicationUser entity);
        ApplicationUser Update(ApplicationUser entity);
        ApplicationUser Delete(string id);
    }
}
