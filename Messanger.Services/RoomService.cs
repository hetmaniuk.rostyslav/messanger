﻿using Messanger.Persistance.Entities;
using Messanger.Persistance.Entities.Chat;
using Messanger.Persistance.Infrastructure;
using Messanger.Persistance.Repos.Interfaces;
using Messanger.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Messanger.Services
{
    public class RoomService : IRoomService
    {
        private readonly IRoomRepository _roomRepository;
        private readonly IApplicationUserRepository _applicationUserRepository;

        public RoomService(IRoomRepository roomRepository, IApplicationUserRepository applicationUserRepository)
        {
            _roomRepository = roomRepository;
            _applicationUserRepository = applicationUserRepository; 
        }

        public Room Get(string id)
        {
            return _roomRepository.FindOne(x => x.Id == id);
        }

        public IEnumerable<Room> GetMany()
        {
            return _roomRepository.FindMany(x => true);
        }

        public Room Create(Room entity)
        {
            return _roomRepository.InsertOne(entity);
        }

        public Room Update(Room entity)
        {
            var room = _roomRepository.FindOne(x => x.Id == entity.Id);
            if (room.ParticipantLinks == null) 
                room.ParticipantLinks = new List<ApplicationUserRoom>();

            entity.ParticipantLinks.ForEach(x => 
            {
                var user = _applicationUserRepository.FindOne(s => s.Id == x.UserId);
                if (user == null)
                    throw new DomainException("User not found!");

                if (!room.ParticipantLinks.Any(s => s.UserId == user.Id && s.Room.Id == room.Id))
                {
                    room.ParticipantLinks.Add(new ApplicationUserRoom
                    {
                        Room = room,
                        User = user
                    });
                }
            });

            return _roomRepository.UpdateOne(room);
        }

        public Room Delete(string id)
        {
            return _roomRepository.DeleteOne(x => x.Id == id);
        }
    }
}
