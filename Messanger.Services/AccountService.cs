﻿using Messanger.Persistance.Entities;
using Messanger.Persistance.Infrastructure;
using Messanger.Persistance.Models;
using Messanger.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Services
{
    public class AccountService : IAccountService
    {
        private readonly IApplicationUserService _applicationUserService;
        public AccountService(IApplicationUserService applicationUserService)
        {
            _applicationUserService = applicationUserService;
        }

        public ApplicationUser Login(LoginModel loginModel)
        {
            var user = _applicationUserService.GetByEmail(loginModel.Email);
            if (user == null)
                throw new DomainException("Email is incorrect!");

            if (user.Password != loginModel.Password)
                throw new DomainException("Password is incorrect!");

            return user;
        }

        public bool Register(RegisterModel loginModel)
        {
            var existedUser = _applicationUserService.GetByEmail(loginModel.Email);
            if (existedUser != null)
                throw new DomainException("Email is already taken!");

            var user = new ApplicationUser
            {
                Name = loginModel.Name,
                Email = loginModel.Email,
                Password = loginModel.Password
            };
            return _applicationUserService.Create(user) != null;
        }
    }
}
