﻿using Messanger.Persistance.Entities.Chat;
using Messanger.Persistance.Repos.Interfaces;
using Messanger.Services.Interfaces;

namespace Messanger.Services
{
    public class MessageService : IMessageService
    {
        private readonly IMessageRepository _messageRepository;

        public MessageService(IMessageRepository messageRepository)
        {
            _messageRepository = messageRepository;
        }

        public Message Get(string id)
        {
            return _messageRepository.FindOne(x => x.Id == id);
        }

        public Message Create(Message entity)
        {
            return _messageRepository.InsertOne(entity);
        }

        public Message Update(Message entity)
        {
            return _messageRepository.UpdateOne(entity);
        }

        public Message Delete(string id)
        {
            return _messageRepository.DeleteOne(x => x.Id == id);
        }
    }
}
