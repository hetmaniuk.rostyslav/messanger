﻿using Autofac;
using JKang.IpcServiceFramework;
using Messanger.Persistance.Infrastructure;
using Messanger.Persistance.Repos.Interfaces;
using Messanger.Protocol;
using Messanger.Server.Controllers;
using Messanger.Server.Injector;
using Microsoft.EntityFrameworkCore;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Messanger.Server
{
    public class ApplicationStarter
    {
        private static ApplicationStarter Instance;

        private ApplicationStarter()
        {
            Start();
        }

        public static InjectionContainer InjectionContainer { get; set; }

        private void Start()
        {
            Console.WriteLine("Initialize container!");

            InjectionContainer = Activator.CreateInstance<InjectionContainer>();

            InjectionContainer.ConfigureServices();

            Console.WriteLine("Migrate database!");

            using(var scope = InjectionContainer.GetContainer().BeginLifetimeScope())
            {
                var context = scope.Resolve<MessangerDbContext>();
                context.Database.Migrate();
            }

            Console.WriteLine("Handler succssesfully started!");
        }

        public void Stop()
        {
            InjectionContainer = null;
            Console.WriteLine("Handler succssesfully stoped!");
        }

        public T ResolveController<T>() where T : ControllerBase
        {
            var container = InjectionContainer.GetContainer();
            using (var scope = container.BeginLifetimeScope())
            {
                return scope.Resolve<T>();
            }
        }


        public static ApplicationStarter GetInstance()
        {
            if (Instance == null)
                Instance = new ApplicationStarter();
            return Instance;
        }
    }
}
