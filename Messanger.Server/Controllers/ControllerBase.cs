﻿using Messanger.Persistance.Infrastructure;
using Messanger.Persistance.Infrastructure.Interfaces;
using Messanger.Protocol;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Server.Controllers
{
    public abstract class ControllerBase
    {
        protected string ToResponce<TEntity>(TEntity entity) where TEntity : IEntity
        {
            return JsonConvert.SerializeObject(new ApiResult<TEntity>(entity), GetSerializerSettings());
        }

        protected string ToResponce<TEntity>(IEnumerable<TEntity> entity) where TEntity : IEntity
        {
            return JsonConvert.SerializeObject(new ApiResult<IEnumerable<TEntity>>(entity), GetSerializerSettings());
        }

        protected string ToResponce(bool value)
        {
            return JsonConvert.SerializeObject(new ApiResult<bool>(value), GetSerializerSettings());
        }

        protected string ToErrorResponce<TEntity>(string message)
        {
            return JsonConvert.SerializeObject(new ApiResult<TEntity>(errorMessage: message), GetSerializerSettings());
        }

        protected string ExecuteRequest<TEntity>(Func<string> action)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Execute {this.GetType().Name} request!");
            Console.ResetColor();
            try
            {
                return action.Invoke();
            }
            catch(DomainException ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Error stack trace {ex.Error}");
                return ToErrorResponce<TEntity>(ex.Error);
            }
        }

        private JsonSerializerSettings GetSerializerSettings()
        {
            return new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
        }
    }
}
