﻿using Messanger.Persistance.Entities;
using Messanger.Persistance.Infrastructure;
using Messanger.Persistance.Repos.Interfaces;
using Messanger.Protocol;
using Messanger.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Server.Controllers.V1
{
    public class ApplicationUserController : ControllerBase
    {
        private readonly IApplicationUserService _applicationUserService;
        public ApplicationUserController(IApplicationUserService applicationUserService)
        {
            _applicationUserService = applicationUserService;
        }

        public string Get(string id)
        {
            return ExecuteRequest<string>(() =>
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new DomainException("Invalid id!");

                var result = _applicationUserService.Get(id);
                return ToResponce(result);
            });
        }

        public string Delete(string id)
        {
            return ExecuteRequest<string>(() =>
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new DomainException("Invalid id!");

                var result = _applicationUserService.Delete(id);
                return ToResponce(result);
            });
        }
    }
}
