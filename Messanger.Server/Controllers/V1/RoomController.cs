﻿using Messanger.Persistance.Entities;
using Messanger.Persistance.Entities.Chat;
using Messanger.Persistance.Infrastructure;
using Messanger.Protocol;
using Messanger.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Server.Controllers.V1
{
    public class RoomController : ControllerBase
    {
        private readonly IRoomService _roomService;

        public RoomController(IRoomService roomService)
        {
            _roomService = roomService;
        }

        public string Get(string id)
        {
            return ExecuteRequest<string>(() =>
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new DomainException("Invalid id!");

                var result = _roomService.Get(id);
                return ToResponce(result);
            });
        }

        public string GetMany()
        {
            return ExecuteRequest<string>(() =>
            {
                var result = _roomService.GetMany();
                return ToResponce(result);
            });
        }

        public string Create(Room entity)
        {
            return ExecuteRequest<string>(() =>
            {
                if (!entity.Validate())
                    throw new DomainException("Invalid room!");

                var result = _roomService.Create(entity);
                return ToResponce(result);
            });
        }

        public string Update(Room entity)
        {
            return ExecuteRequest<string>(() =>
            {
                if (!entity.Validate())
                    throw new DomainException("Invalid room!");

                var result = _roomService.Update(entity);
                return ToResponce(result);
            });
        }

        public string Delete(string id)
        {
            return ExecuteRequest<string>(() =>
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new DomainException("Invalid id");

                var result = _roomService.Delete(id);
                return ToResponce(result);
            });
        }
    }
}
