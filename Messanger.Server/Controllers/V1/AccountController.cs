﻿using Messanger.Persistance.Entities;
using Messanger.Persistance.Infrastructure;
using Messanger.Persistance.Models;
using Messanger.Protocol;
using Messanger.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Server.Controllers.V1
{
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public string Login(LoginModel model)
        {
            return ExecuteRequest<string>(() =>
            {
                if (!model.Validate())
                    throw new DomainException("Parametrs is invalid!");

                var result = _accountService.Login(model);
                return ToResponce(result);
            });
        }

        public string Register(RegisterModel model)
        {
            var res = ExecuteRequest<string>(() =>
            {
                if (!model.Validate())
                    throw new DomainException("Parametrs is invalid!");

                var result = _accountService.Register(model);
                return ToResponce(result);
            });
            return res;
        }
    }
}
