﻿using Messanger.Persistance.Entities.Chat;
using Messanger.Persistance.Infrastructure;
using Messanger.Protocol;
using Messanger.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Server.Controllers.V1
{
    public class MessageController : ControllerBase
    {
        private readonly IMessageService _messageService;

        public MessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }
        public string Get(string id)
        {
            return ExecuteRequest<string>(() =>
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new DomainException("Invalid id");

                var result = _messageService.Get(id);
                return ToResponce(result);
            });
        }

        public string Create(Message entity)
        {
            return ExecuteRequest<string>(() =>
            {
                if (!entity.Validate())
                    throw new DomainException("Invalid message");

                var result = _messageService.Create(entity);
                return ToResponce(result);
            });
        }

        public string Update(Message entity)
        {
            return ExecuteRequest<string>(() =>
            {
                if (!entity.Validate())
                    throw new DomainException("Invalid message");

                var result = _messageService.Update(entity);
                return ToResponce(result);
            });
        }

        public string Delete(string id)
        {
            return ExecuteRequest<string>(() =>
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new DomainException("Invalid id");

                var result = _messageService.Delete(id);
                return ToResponce(result);
            });
        }
    }
}
