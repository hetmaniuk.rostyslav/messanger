﻿using Autofac;
using Messanger.Persistance.Infrastructure;
using Messanger.Persistance.Repos;
using Messanger.Persistance.Repos.Interfaces;
using Messanger.Server.Controllers;
using Messanger.Server.Controllers.V1;
using Messanger.Services;
using Messanger.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Server.Injector
{
    public class InjectionContainer
    {
        private static IContainer _container;

        public InjectionContainer()
        {

        }

        public void ConfigureServices()
        {
            Console.WriteLine("Initialize services!");

            var builder = new ContainerBuilder();

            builder.RegisterType<MessangerDbContext>().SingleInstance();
            builder.RegisterType<ApplicationUserRepository>().As<IApplicationUserRepository>();
            builder.RegisterType<RoomRepository>().As<IRoomRepository>();
            builder.RegisterType<MessageRepository>().As<IMessageRepository>();

            builder.RegisterType<ApplicationUserService>().As<IApplicationUserService>();
            builder.RegisterType<AccountService>().As<IAccountService>();
            builder.RegisterType<MessageService>().As<IMessageService>();
            builder.RegisterType<RoomService>().As<IRoomService>();

            builder.RegisterType<ApplicationUserController>().InstancePerLifetimeScope();
            builder.RegisterType<AccountController>().InstancePerLifetimeScope();
            builder.RegisterType<MessageController>().InstancePerLifetimeScope();
            builder.RegisterType<RoomController>().InstancePerLifetimeScope();

            _container = builder.Build();
        }

        public IContainer GetContainer() => _container;
    }
}
