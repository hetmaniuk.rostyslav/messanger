﻿using Messanger.Client.Constants;
using Messanger.Client.CustomControls;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Messanger.Client.Extensions
{
    public static class UITabControl
    {
        public static void SetupUI(this CustomTabControl control)
        {
            control.DrawMode = TabDrawMode.OwnerDrawFixed;
            control.BackColor = Colors.Container.BackgroundColor;
            control.BackTabColor = Colors.Tab.BackColor;
            control.BackSelectedTabColor = Colors.Tab.SelectedColor;
            control.TabForeColor = Colors.Tab.ForeColor;
            foreach (TabPage item in control.TabPages)
            {
                item.SetupUI();
            }
        }
    }
}
