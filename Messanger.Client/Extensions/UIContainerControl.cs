﻿using Messanger.Client.Constants;
using Messanger.Client.CustomControls;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Messanger.Client.Extensions
{
    public static class UIContainerControl
    {
        public static void DesignViews(this Control control)
        {
            control.SetupUI();
            switch (control)
            {
                case DataGridView view:
                    view.SetupUI();
                    break;
                case ToolStrip view:
                    view.SetupUI();
                    break;
                case Button view:
                    view.SetupUI();
                    break;
                case CustomTabControl view:
                    view.SetupUI();
                    break;
            }

            foreach (Control item in control.Controls)
            {
                item.DesignViews();
            }
        }

        public static void SetupUI(this Control control)
        {
            control.BackColor = Colors.Container.BackgroundColor;
            control.ForeColor = Colors.Container.ForeColor;
            control.Font = Colors.Fonts.Font;
        }
    }
}
