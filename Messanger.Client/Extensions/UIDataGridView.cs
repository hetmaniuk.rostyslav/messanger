﻿using Messanger.Client.Constants;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Messanger.Client.Extensions
{
    public static class UIDataGridView
    {
        public static void SetupUI(this DataGridView dataGridView)
        {
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView.RowsDefaultCellStyle = new DataGridViewCellStyle()
            {
                SelectionBackColor = Colors.DataGrid.SelectionBackColor,
                SelectionForeColor = Colors.DataGrid.SelectionForeColor,
                BackColor = Colors.DataGrid.BackColor,
                ForeColor = Colors.DataGrid.ForeColor,
                Font = Colors.Fonts.Font
            };
            dataGridView.AlternatingRowsDefaultCellStyle = new DataGridViewCellStyle()
            {
                SelectionBackColor = Colors.DataGrid.SelectionBackColor,
                SelectionForeColor = Colors.DataGrid.SelectionForeColor,
                BackColor = Colors.DataGrid.AlternativeBackColor,
                ForeColor = Colors.DataGrid.ForeColor,
                Font = Colors.Fonts.Font
            };
            dataGridView.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            dataGridView.GridColor = Colors.DataGrid.GridColor;
            dataGridView.BackgroundColor = Colors.DataGrid.BackgroundColor;

            dataGridView.ColumnHeadersDefaultCellStyle = new DataGridViewCellStyle()
            {
                ForeColor = Colors.DataGrid.ForeColor,
                BackColor = Colors.DataGrid.BackgroundColor,
                SelectionBackColor = Colors.DataGrid.BackgroundColor,
                SelectionForeColor = Colors.DataGrid.ForeColor,
                Font = Colors.Fonts.Font
            };

            dataGridView.RowHeadersDefaultCellStyle = new DataGridViewCellStyle()
            {
                BackColor = Colors.DataGrid.BackgroundColor,
                Font = Colors.Fonts.Font,
                SelectionBackColor = Colors.DataGrid.BackgroundColor,
                SelectionForeColor = Colors.DataGrid.ForeColor,
            };
            dataGridView.AllowUserToResizeRows = false;
            dataGridView.BorderStyle = BorderStyle.None;
            dataGridView.RowHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dataGridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dataGridView.EnableHeadersVisualStyles = false;
        }

        public static void RefreshGridView(this DataGridView gridView, int rowCount)
        {
            if (gridView.RowCount != rowCount)
                gridView.RowCount = rowCount;
            else
                gridView.Refresh();
        }
    }
}
