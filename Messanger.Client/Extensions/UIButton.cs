﻿using Messanger.Client.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Messanger.Client.Extensions
{
    public static class UIButton
    {
        public static void SetupUI(this Button button)
        {
            button.FlatStyle = FlatStyle.Flat;
            button.FlatAppearance.BorderColor = Colors.Container.ForeColor;
            button.FlatAppearance.BorderSize = 1;
        }
    }
}
