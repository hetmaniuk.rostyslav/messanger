﻿using Messanger.Client.CustomControls;
using System;
using System.Windows.Forms;

namespace Messanger
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvAllRooms = new System.Windows.Forms.DataGridView();
            this.colNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAllName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAllParticipants = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMyParticipants = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAllJoin = new System.Windows.Forms.DataGridViewButtonColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.btnCreateRoom = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.tabControl = new CustomTabControl();
            this.myRoomTab = new System.Windows.Forms.TabPage();
            this.dgvMyRooms = new System.Windows.Forms.DataGridView();
            this.colMyCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMyRoomName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.collUnreadMessage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.allRoomTab = new System.Windows.Forms.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllRooms)).BeginInit();
            this.tabControl.SuspendLayout();
            this.myRoomTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMyRooms)).BeginInit();
            this.allRoomTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvAllRooms
            // 
            this.dgvAllRooms.AllowUserToAddRows = false;
            this.dgvAllRooms.AllowUserToDeleteRows = false;
            this.dgvAllRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAllRooms.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colNum,
            this.colAllName,
            this.colAllParticipants,
            this.colAllJoin});
            this.dgvAllRooms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAllRooms.Location = new System.Drawing.Point(3, 3);
            this.dgvAllRooms.Name = "dgvAllRooms";
            this.dgvAllRooms.ReadOnly = true;
            this.dgvAllRooms.RowHeadersVisible = false;
            this.dgvAllRooms.Size = new System.Drawing.Size(882, 445);
            this.dgvAllRooms.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAllRooms.TabIndex = 0;
            this.dgvAllRooms.VirtualMode = true;
            this.dgvAllRooms.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAllRooms_CellDoubleClick);
            this.dgvAllRooms.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.dgvAllRooms_CellValueNeeded);
            // 
            // colNum
            // 
            this.colNum.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.colNum.HeaderText = "#";
            this.colNum.Name = "colNum";
            this.colNum.ReadOnly = true;
            this.colNum.Width = 39;
            // 
            // colAllName
            // 
            this.colAllName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colAllName.HeaderText = "Room name";
            this.colAllName.Name = "colAllName";
            this.colAllName.ReadOnly = true;
            // 
            // colMyParticipants
            // 
            this.colMyParticipants.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colMyParticipants.FillWeight = 20F;
            this.colMyParticipants.HeaderText = "Participants";
            this.colMyParticipants.Name = "colMyParticipants";
            this.colMyParticipants.ReadOnly = true;
            // 
            // colAllJoin
            // 
            this.colAllJoin.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colAllJoin.FillWeight = 20F;
            this.colAllJoin.HeaderText = "Join Room";
            this.colAllJoin.Name = "colAllJoin";
            this.colAllJoin.ReadOnly = true;
            this.colAllJoin.Text = "Join";
            this.colAllJoin.UseColumnTextForButtonValue = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Email:";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(50, 13);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(38, 13);
            this.lblName.TabIndex = 3;
            this.lblName.Text = "Name:";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(50, 39);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(38, 13);
            this.lblEmail.TabIndex = 4;
            this.lblEmail.Text = "Name:";
            // 
            // btnCreateRoom
            // 
            this.btnCreateRoom.Location = new System.Drawing.Point(12, 75);
            this.btnCreateRoom.Name = "btnCreateRoom";
            this.btnCreateRoom.Size = new System.Drawing.Size(188, 25);
            this.btnCreateRoom.TabIndex = 5;
            this.btnCreateRoom.Text = "Create room";
            this.btnCreateRoom.UseVisualStyleBackColor = true;
            this.btnCreateRoom.Click += new System.EventHandler(this.btnCreateRoom_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.Location = new System.Drawing.Point(12, 137);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(188, 25);
            this.btnLogout.TabIndex = 6;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(12, 106);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(188, 25);
            this.btnRefresh.TabIndex = 7;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.myRoomTab);
            this.tabControl.Controls.Add(this.allRoomTab);
            this.tabControl.Location = new System.Drawing.Point(206, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(597, 329);
            this.tabControl.TabIndex = 8;
            // 
            // myRoomTab
            // 
            this.myRoomTab.Controls.Add(this.dgvMyRooms);
            this.myRoomTab.Location = new System.Drawing.Point(4, 22);
            this.myRoomTab.Name = "myRoomTab";
            this.myRoomTab.Padding = new System.Windows.Forms.Padding(3);
            this.myRoomTab.Size = new System.Drawing.Size(589, 303);
            this.myRoomTab.TabIndex = 1;
            this.myRoomTab.Text = "My Rooms";
            this.myRoomTab.UseVisualStyleBackColor = true;
            // 
            // dgvMyRooms
            // 
            this.dgvMyRooms.AllowUserToAddRows = false;
            this.dgvMyRooms.AllowUserToDeleteRows = false;
            this.dgvMyRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMyRooms.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colMyCount,
            this.colMyRoomName,
            this.colMyParticipants,
            this.collUnreadMessage});
            this.dgvMyRooms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMyRooms.Location = new System.Drawing.Point(3, 3);
            this.dgvMyRooms.Name = "dgvMyRooms";
            this.dgvMyRooms.ReadOnly = true;
            this.dgvMyRooms.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMyRooms.RowHeadersVisible = false;
            this.dgvMyRooms.Size = new System.Drawing.Size(583, 297);
            this.dgvMyRooms.TabIndex = 1;
            this.dgvMyRooms.VirtualMode = true;
            this.dgvMyRooms.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMyRooms_CellDoubleClick);
            this.dgvMyRooms.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.dgvMyRooms_CellValueNeeded);
            // 
            // colMyCount
            // 
            this.colMyCount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.colMyCount.HeaderText = "#";
            this.colMyCount.Name = "colMyCount";
            this.colMyCount.ReadOnly = true;
            this.colMyCount.Width = 39;
            // 
            // colMyRoomName
            // 
            this.colMyRoomName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colMyRoomName.HeaderText = "Room name";
            this.colMyRoomName.Name = "colMyRoomName";
            this.colMyRoomName.ReadOnly = true;
            // 
            // colAllParticipants
            // 
            this.colAllParticipants.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colAllParticipants.FillWeight = 20F;
            this.colAllParticipants.HeaderText = "Participants";
            this.colAllParticipants.Name = "colAllParticipants";
            this.colAllParticipants.ReadOnly = true;
            // 
            // collUnreadMessage
            // 
            this.collUnreadMessage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.collUnreadMessage.FillWeight = 20F;
            this.collUnreadMessage.HeaderText = "Messages";
            this.collUnreadMessage.Name = "collUnreadMessage";
            this.collUnreadMessage.ReadOnly = true;
            this.collUnreadMessage.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.collUnreadMessage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // allRoomTab
            // 
            this.allRoomTab.Controls.Add(this.dgvAllRooms);
            this.allRoomTab.Location = new System.Drawing.Point(4, 22);
            this.allRoomTab.Name = "allRoomTab";
            this.allRoomTab.Padding = new System.Windows.Forms.Padding(3);
            this.allRoomTab.Size = new System.Drawing.Size(888, 451);
            this.allRoomTab.TabIndex = 0;
            this.allRoomTab.Text = "All Rooms";
            this.allRoomTab.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 353);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.btnCreateRoom);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllRooms)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.myRoomTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMyRooms)).EndInit();
            this.allRoomTab.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAllRooms;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Button btnCreateRoom;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Button btnRefresh;
        private CustomTabControl tabControl;
        private System.Windows.Forms.TabPage allRoomTab;
        private System.Windows.Forms.TabPage myRoomTab;
        private System.Windows.Forms.DataGridView dgvMyRooms;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAllName;
        private System.Windows.Forms.DataGridViewButtonColumn colAllJoin;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAllParticipants;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMyCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMyRoomName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMyParticipants;
        private System.Windows.Forms.DataGridViewTextBoxColumn collUnreadMessage;
    }
}