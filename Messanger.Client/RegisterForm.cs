﻿using Messanger.Client.Core;
using Messanger.Client.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Messanger
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
            this.DesignViews();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ShowLogin();
        }

        private void ShowLogin()
        {
            var form = new LoginForm();
            form.Show();
            this.Hide();
        }

        private async void btnRegister_Click(object sender, EventArgs e)
        {
            var result = await AccountApi.GetInstance().Register(txtName.Text, txtEmail.Text, txtPassword.Text);
            if (result)
                ShowLogin();

            if (!IsDisposed)
            {
                txtName.Text = string.Empty;
                txtEmail.Text = string.Empty;
                txtPassword.Text = string.Empty;
            }
        }
    }
}
