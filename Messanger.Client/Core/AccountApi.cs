﻿using JKang.IpcServiceFramework;
using Messanger.Persistance.Entities;
using Messanger.Persistance.Entities.Chat;
using Messanger.Persistance.Models;
using Messanger.Protocol;
using Messanger.Protocol.Handlers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Messanger.Client.Core
{
    public class AccountApi
    {
        private static AccountApi _instance = null;
        private readonly IpcServiceClient<IAccountServiceHandler> _accountClient;
        private readonly IpcServiceClient<IApplicationUserServiceHandler> _userClient;

        private ApplicationUser _currentUser;

        private AccountApi()
        {
            _accountClient = new IpcServiceClientBuilder<IAccountServiceHandler>()
                                                    .UseNamedPipe("account")
                                                    .Build();

            _userClient = new IpcServiceClientBuilder<IApplicationUserServiceHandler>()
                                                    .UseNamedPipe("account")
                                                    .Build();
        }

        public static AccountApi GetInstance()
        {
            if (_instance == null)
                _instance = new AccountApi();
            return _instance;
        }

        public ApplicationUser GetCurrentUser() =>
            _currentUser;

        public async Task<ApplicationUser> Login(string email, string password)
        {
            var loginModel = new LoginModel { Email = email, Password = password };
            var responce = await _accountClient.InvokeAsync(x => x.Login(loginModel));

            if (string.IsNullOrWhiteSpace(responce))
                return null;

            var result = JsonConvert.DeserializeObject<ApiResult<ApplicationUser>>(responce);
            if (result.Result == null)
            {
                MessageBox.Show(result.ErrorMessage, "Error!");
                return null;
            }
            _currentUser = result.Result;

            return result.Result;
        }

        public async Task<bool> Register(string name, string email, string password)
        {
            var registerModel = new RegisterModel { Name = name, Email = email, Password = password };
            var responce = await _accountClient.InvokeAsync(x => x.Register(registerModel));

            if (string.IsNullOrWhiteSpace(responce))
                return false;

            var result = JsonConvert.DeserializeObject<ApiResult<bool>>(responce);
            if (result.Result == false)
            {
                MessageBox.Show(result.ErrorMessage, "Error!");
            }
            return result.Result;
        }

        public async Task<ApplicationUser> JoinToRooms(Room room)
        {
            var user = GetCurrentUser();

            if (user.RoomLinks == null)
                user.RoomLinks = new List<ApplicationUserRoom>();

            user.RoomLinks.Add(new ApplicationUserRoom
            {
                RoomId = room.Id,
                UserId = user.Id
            });

            var responce = await _userClient.InvokeAsync(x => x.Update(user));
            if (string.IsNullOrWhiteSpace(responce))
                return null;

            var result = JsonConvert.DeserializeObject<ApiResult<ApplicationUser>>(responce);
            if (result.Result == null)
            {
                MessageBox.Show(result.ErrorMessage, "Error!");
                return null;
            }
            _currentUser = result.Result;

            return result.Result;
        }

        public bool Logout()
        {
            _currentUser = null;
            return _currentUser == null;
        }
    }
}
