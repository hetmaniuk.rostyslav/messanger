﻿using JKang.IpcServiceFramework;
using Messanger.Persistance.Entities.Chat;
using Messanger.Protocol;
using Messanger.Protocol.Handlers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Messanger.Client.Core
{
    public class RoomApi
    {
        private static RoomApi _instance = null;
        private readonly IpcServiceClient<IRoomServiceHandler> _client;

        private RoomApi()
        {
            _client = new IpcServiceClientBuilder<IRoomServiceHandler>()
                                                    .UseNamedPipe("room")
                                                    .Build();
        }

        public static RoomApi GetInstance()
        {
            if (_instance == null)
                _instance = new RoomApi();
            return _instance;
        }

        public async Task<Room> Create(Room room)
        {
            var responce = await _client.InvokeAsync(x => x.Create(room));

            if (string.IsNullOrWhiteSpace(responce))
                return null;

            var result = JsonConvert.DeserializeObject<ApiResult<Room>>(responce);
            if (result.Result == null)
            {
                MessageBox.Show(result.ErrorMessage, "Error!");
                return null;
            }
            return result.Result;
        }

        public async Task<Room> Update(Room room)
        {
            var responce = await _client.InvokeAsync(x => x.Update(room));

            if (string.IsNullOrWhiteSpace(responce))
                return null;

            var result = JsonConvert.DeserializeObject<ApiResult<Room>>(responce);
            if (result.Result == null)
            {
                MessageBox.Show(result.ErrorMessage, "Error!");
                return null;
            }
            return result.Result;
        }

        public async Task<IEnumerable<Room>> GetAllRooms()
        {
            var responce = await _client.InvokeAsync(x => x.GetMany());

            if (string.IsNullOrWhiteSpace(responce))
                return null;

            var result = JsonConvert.DeserializeObject<ApiResult<IEnumerable<Room>>>(responce);
            if (result.Result == null)
            {
                MessageBox.Show(result.ErrorMessage, "Error!");
                return null;
            }
            return result.Result;
        }
    }
}
