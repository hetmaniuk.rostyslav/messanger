﻿using Messanger.Client.Core;
using Messanger.Client.Extensions;
using Messanger.Persistance.Entities;
using Messanger.Persistance.Entities.Chat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Messanger
{
    public partial class AddRoom : Form
    {
        private readonly RoomApi _roomApi;
        private readonly AccountApi _accountApi;
        public AddRoom()
        {
            InitializeComponent();
            _roomApi = RoomApi.GetInstance();
            _accountApi = AccountApi.GetInstance();

            this.DesignViews();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private async void btnAddRoom_Click(object sender, EventArgs e)
        {
            var room = new Room
            {
                Name = txtName.Text
            };
            room = await _roomApi.Create(room);

            room.ParticipantLinks = new List<ApplicationUserRoom>
                {
                    new ApplicationUserRoom
                    {
                        UserId = _accountApi.GetCurrentUser().Id,
                        RoomId = room.Id
                    }
                };

            var updateResult = await _roomApi.Update(room);
            if (updateResult != null)
                DialogResult = DialogResult.OK;
            this.Hide();
        }
    }
}
