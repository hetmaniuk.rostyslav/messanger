﻿using Messanger.Client.Core;
using Messanger.Client.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Messanger
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
            this.DesignViews();
        }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            var result = await AccountApi.GetInstance().Login(txtEmail.Text, txtPassword.Text);
            if (result == null) return;

            var form = new MainForm();
            form.Show();
            this.Hide();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            var form = new RegisterForm();
            form.Show();
            this.Hide();
        }
    }
}
