﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Messanger.Client.Constants
{
    public class Colors
    {
        public class Fonts
        {
            public static readonly Font Font = new System.Drawing.Font("Arial-Bold", 11);
        }

        public class DataGrid
        {
            public static readonly Color ForeColor = Color.FromArgb(228, 233, 235);
            public static readonly Color SelectionForeColor = Color.FromArgb(228, 233, 235);
            public static readonly Color SelectionBackColor = Color.FromArgb(118, 127, 136);
            public static readonly Color BackgroundColor = Color.FromArgb(12, 9, 11);
            public static readonly Color BackColor = Color.FromArgb(71, 65, 69);
            public static readonly Color AlternativeBackColor = Color.FromArgb(49, 43, 47);
            public static readonly Color GridColor = Color.FromArgb(105, 105, 105);
        }

        public class Container
        {
            public static readonly Color BackgroundColor = Color.FromArgb(34, 32, 34);
            public static readonly Color ForeColor = Color.FromArgb(228, 233, 235);
        }

        public class Tab
        {
            public static readonly Color BackgroundColor = Color.FromArgb(34, 32, 34);
            public static readonly Color ForeColor = Color.FromArgb(228, 233, 235);
            public static readonly Color BackColor = Color.FromArgb(78, 76, 78);
            public static readonly Color SelectedColor = Color.FromArgb(118, 127, 136);
        }
    }
}
