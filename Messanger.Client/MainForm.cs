﻿using Messanger.Client.Core;
using Messanger.Client.Extensions;
using Messanger.Persistance.Entities;
using Messanger.Persistance.Entities.Chat;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Messanger
{
    public partial class MainForm : Form
    {
        private readonly List<Room> _allRooms = new List<Room>();
        private readonly List<Room> _myRooms = new List<Room>();

        private readonly RoomApi _roomApi;
        private readonly AccountApi _accountApi;

        private ApplicationUser _user;

        public MainForm()
        {
            InitializeComponent();

            _roomApi = RoomApi.GetInstance();
            _accountApi = AccountApi.GetInstance();

            btnRefresh.Click += BtnRefresh_Click;

            this.DesignViews();
        }

        private async void MainForm_Load(object sender, EventArgs e)
        {
            _user = _accountApi.GetCurrentUser();
            if (_user == null)
            {
                MessageBox.Show("User is not logined!", "Error!");
                this.Close();
                return;
            }
            lblName.Text = _user.Name;
            lblEmail.Text = _user.Email;

            await RefreshAll();
        }

        private async Task RefreshAll()
        {
            await LoadRooms();

            RefreshAllRooms();
            RefreshMyRooms();
        }

        private void RefreshAllRooms()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(RefreshAllRooms));
                return;
            }

            dgvAllRooms.RefreshGridView(_allRooms.Count);
        }

        private void RefreshMyRooms()
        {
            if (InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(RefreshMyRooms));
                return;
            }

            dgvMyRooms.RefreshGridView(_myRooms.Count);
        }

        private async Task LoadRooms()
        {
            var listRoom = await _roomApi.GetAllRooms();

            _myRooms.Clear();
            _myRooms.AddRange(listRoom.Where(x => x.ParticipantLinks != null && 
                                                x.ParticipantLinks.Any(s => 
                                                            s.UserId == _user.Id)));

            _allRooms.Clear();
            _allRooms.AddRange(listRoom.Where(x => x.ParticipantLinks == null || 
                                                !(x.ParticipantLinks != null &&
                                                x.ParticipantLinks.Any(s => 
                                                            s.UserId == _user.Id))));
        }

        private async void btnCreateRoom_Click(object sender, EventArgs e)
        {
            var form = new AddRoom();
            
            if (form.ShowDialog() == DialogResult.OK)
            {
                await RefreshAll();
            }
        }


        private async void BtnRefresh_Click(object sender, EventArgs e)
        {
            await LoadRooms();

            RefreshAllRooms();
            RefreshMyRooms();
        }


        private void btnLogout_Click(object sender, EventArgs e)
        {
            if (_accountApi.Logout())
            {
                var loginForm = new LoginForm();
                loginForm.Show();
                this.Close();
            }
            else
                MessageBox.Show("Something happend, try again!", "Error!");
        }

        private void dgvMyRooms_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            if (e.RowIndex < 0 || e.RowIndex >= _myRooms.Count)
                return;

            var room = _myRooms[e.RowIndex];

            if (colNum.Index == e.ColumnIndex)
                e.Value = e.RowIndex;
            else if (colAllName.Index == e.ColumnIndex)
                e.Value = room.Name;
            else if (colMyParticipants.Index == e.ColumnIndex)
                e.Value = room.ParticipantLinks.Count;
            else if (collUnreadMessage.Index == e.ColumnIndex)
                e.Value = room.Messages.Count;
        }

        private void dgvMyRooms_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && _myRooms.Count > e.RowIndex)
            {
                var form = new RoomForm();
                form.SetRoom(_myRooms[e.RowIndex]);
                form.Show();
            }
        }

        private void dgvAllRooms_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            if (e.RowIndex < 0 || e.RowIndex >= _allRooms.Count)
                return;

            var room = _allRooms[e.RowIndex];

            if (colNum.Index == e.ColumnIndex)
                e.Value = e.RowIndex;
            else if (colAllName.Index == e.ColumnIndex)
                e.Value = room.Name;
            else if (colAllParticipants.Index == e.ColumnIndex)
                e.Value = room.ParticipantLinks.Count;
        }

        private async void dgvAllRooms_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && _allRooms.Count > e.RowIndex)
            {
                if (colAllJoin.Index == e.ColumnIndex)
                {
                    var room = _allRooms[e.RowIndex];
                    room.ParticipantLinks.Add(new ApplicationUserRoom { RoomId = room.Id, UserId = _user.Id });
                    await _roomApi.Update(room);

                    await RefreshAll();
                }
            }            
        }
    }
}
