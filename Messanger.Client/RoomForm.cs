﻿using Messanger.Client.Core;
using Messanger.Client.Extensions;
using Messanger.Persistance.Entities;
using Messanger.Persistance.Entities.Chat;
using Messanger.Protocol;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebSocket4Net;
using Message = Messanger.Persistance.Entities.Chat.Message;

namespace Messanger
{
    public partial class RoomForm : Form
    {
        private Room _room { get; set; }
        private Socket _socket;

        private static int _bufferSize = 8 * 1024;
        private byte[] _buffer = new byte[_bufferSize];


        private ApplicationUser _user;

        public RoomForm()
        {
            InitializeComponent();
            this.DesignViews();
            this.Load += RoomForm_Load;
        }

        public void SetRoom(Room room)
        {
            _room = room;

            _user = AccountApi.GetInstance().GetCurrentUser();

            this.Text = _room.Name;

            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
   
            _socket.BeginConnect("127.0.0.1", 8, ConnectCallback, null);
        }


        private void ConnectCallback(IAsyncResult AR)
        {
            try
            {
                _socket.EndConnect(AR);
                _socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, ReceiveCallback, null);
            }
            catch (SocketException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (ObjectDisposedException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ReceiveCallback(IAsyncResult AR)
        {
            try
            {
                int received = _socket.EndReceive(AR);

                if (received == 0)
                {
                    return;
                }

                var buffer = new byte[received];
                Array.Copy(_buffer, buffer, received);
                var json = Encoding.ASCII.GetString(buffer);

                var message = JsonConvert.DeserializeObject<ApiResult<Message>>(json);
                _room.Messages.Add(message.Result);
                UpdateGrid();

                _socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, ReceiveCallback, null);
            }
            catch (SocketException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (ObjectDisposedException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void UpdateGrid()
        {
            if(InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(UpdateGrid));
                return;
            }

            if (dgvMessages.RowCount != _room.Messages.Count)
                dgvMessages.RowCount = _room.Messages.Count;
            else
                dgvMessages.Refresh();
        }

        private void dgvMessages_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            if (e.RowIndex < 0 || e.RowIndex >= _room.Messages.Count)
                return;

            var message = _room.Messages[e.RowIndex];

            if (colSender.Index == e.ColumnIndex)
                e.Value = message.Owner.Name;
            else if (colMessage.Index == e.ColumnIndex)
                e.Value = message.Text;
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            var message = new Message();
            message.RoomId = _room.Id;
            message.OwnerId = _user.Id;
            message.Text = textBox1.Text;
            var json = JsonConvert.SerializeObject(message);
            var data = Encoding.ASCII.GetBytes(json);
            _socket.Send(data);
            textBox1.Text = string.Empty;
        }

        private void RoomForm_Load(object sender, EventArgs e)
        {
            var data = Encoding.ASCII.GetBytes($"roomId|{_room.Id}");
            _socket.Send(data);
        }

    }
}
