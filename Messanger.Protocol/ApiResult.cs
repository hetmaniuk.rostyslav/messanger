﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Protocol
{
    public class ApiResult<T>
    {
        public ApiResult()
        {

        }
        
        public ApiResult(T result) : base()
        {
            Result = result;
        }

        public ApiResult(string errorMessage) : base()
        {
            ErrorMessage = errorMessage;
        }

        [JsonProperty("Result")]
        public T Result { get; set; } = default(T);

        [JsonProperty("ErrorMessage")]
        public string ErrorMessage { get; set; } = string.Empty;

    }
}
