﻿using Messanger.Persistance.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Protocol.Handlers
{
    public interface IServiceHandler<TEntity> where TEntity : IEntity 
    {
        string Get(string id);
        string Create(TEntity entity);
        string Update(TEntity entity);
        string Delete(string id);
    }
}
