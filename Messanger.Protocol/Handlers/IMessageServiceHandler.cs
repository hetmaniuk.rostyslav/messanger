﻿using Messanger.Persistance.Entities.Chat;

namespace Messanger.Protocol.Handlers
{
    public interface IMessageServiceHandler : IServiceHandler<Message>
    {
    }
}
