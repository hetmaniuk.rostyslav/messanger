﻿using Messanger.Persistance.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Protocol.Handlers
{
    public interface IApplicationUserServiceHandler : IServiceHandler<ApplicationUser>
    {
    }
}
