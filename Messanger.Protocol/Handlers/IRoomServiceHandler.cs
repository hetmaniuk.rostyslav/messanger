﻿using Messanger.Persistance.Entities.Chat;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Protocol.Handlers
{
    public interface IRoomServiceHandler : IServiceHandler<Room>
    {
        string GetMany();
    }
}
