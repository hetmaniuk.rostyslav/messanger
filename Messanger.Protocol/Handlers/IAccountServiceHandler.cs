﻿using Messanger.Persistance.Entities;
using Messanger.Persistance.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Protocol.Handlers
{
    public interface IAccountServiceHandler
    {
        string Login(LoginModel model);
        string Register(RegisterModel model);
    }
}
