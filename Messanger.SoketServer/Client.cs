﻿using Messanger.Persistance.Entities.Chat;
using Messanger.Server;
using Messanger.Server.Controllers.V1;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Messanger.SoketServer
{
    public class Client
    {
        public string Id { get; private set; }

        public string RoomId { get; private set; }
        private readonly MessageController _messageController;

        public IPEndPoint EndPoint { get; private set; }

        private readonly Socket _socket;

        private static int _messageSize => 8 * 1024;

        private byte[] _dump = new byte[_messageSize];

        public Client(Socket socket)
        {
            _socket = socket;

            Id = Guid.NewGuid().ToString();
            _messageController = ApplicationStarter.GetInstance().ResolveController<MessageController>();
            EndPoint = (IPEndPoint)socket.RemoteEndPoint;

            _socket.BeginReceive(_dump, 0, _dump.Length, SocketFlags.None, ReceiveCallback, null);
        }

        private void ReceiveCallback(IAsyncResult asyncResult)
        {
            try
            {
                var received = _socket.EndReceive(asyncResult);
                var buffer = new byte[received];
                Array.Copy(_dump, buffer, received);

                var text = Encoding.ASCII.GetString(buffer);
                if (text.StartsWith("roomId", StringComparison.InvariantCultureIgnoreCase))
                {
                    var id = text.Split('|')[1];
                    RoomId = id;
                }
                else if(text.StartsWith('{'))
                {
                    var message = JsonConvert.DeserializeObject<Message>(text);
                    var resultMes = _messageController.Create(message);
                    Received?.Invoke(this, resultMes);
                }
                _socket.BeginReceive(_dump, 0, _dump.Length, SocketFlags.None, ReceiveCallback, null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Close();
                Disconnected?.Invoke(this);
            }
        }

        public void Send(string roomId, string text)
        {
            if (RoomId == roomId)
            {
                var data = Encoding.ASCII.GetBytes(text);
                _socket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(SendCallback), null);
            }
        }

        private void SendCallback(IAsyncResult asyncResult)
        {
            _socket.EndSend(asyncResult);
        }

        public void Close()
        {
            _socket.Close();
            _socket.Dispose();   
        }

        public delegate void ClientReceivedHandler(Client sender, string json);
        public delegate void ClientDisconnectedHandler(Client sender);

        public event ClientReceivedHandler Received;
        public event ClientDisconnectedHandler Disconnected;
    }
}
