﻿using Messanger.Persistance.Entities.Chat;
using Messanger.Server;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Messanger.SoketServer
{
   public class Program
   {
        private static Listener _listener;

        private static List<Client> _sockets;

        public static void Main()
        {
            _sockets = new List<Client>();
            Console.WriteLine("Initialize listeners!");
            _listener = new Listener(8);
            _listener.SocketAccepted += _listener_SocketAccepted;

            _listener.Start();
            Console.WriteLine("Start listener!");

            Console.Read();

        }

        private static void _listener_SocketAccepted(Socket socket)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"New connection {socket.RemoteEndPoint}{Environment.NewLine}{DateTime.Now}{Environment.NewLine}============");
            Console.ForegroundColor = ConsoleColor.Gray;

            var client = new Client(socket);
            client.Received += Client_Received;
            client.Disconnected += Client_Disconnected;


            _sockets.Add(client);
        }

        private static void Client_Disconnected(Client sender)
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine($"Disconnected {sender.EndPoint}{Environment.NewLine}{DateTime.Now}{Environment.NewLine}============");
            Console.ForegroundColor = ConsoleColor.Gray;

        }

        private static void Client_Received(Client sender, string json)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine($"Receive client by id: {sender.Id}");
            Console.WriteLine($"Message {json}");
            _sockets.ForEach(x => x.Send(sender.RoomId, json));
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Gray;

        }
    }
}
