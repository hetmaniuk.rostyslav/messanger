﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Messanger.SoketServer
{
    public class Listener : IDisposable
    {
        private Socket _socket;

        public bool Listening { get; private set; }
        
        public int Port { get; private set; }

        public Listener(int port)
        {
            this.Port = port;
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public void Start()
        {
            if (Listening)
                return;

            _socket.Bind(new IPEndPoint(0, Port));
            Console.WriteLine("Binding to endpoint!");
            _socket.Listen(0);
            _socket.BeginAccept(OnResult, null);
            
            Listening = true;
        }

        public void Stop()
        {
            if (!Listening)
                return;

            Console.WriteLine($"Stop socket {_socket.RemoteEndPoint}");

            _socket.Close();
            _socket.Dispose();

            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        private void OnResult(IAsyncResult asyncResult)
        {
            try
            {
                var s = _socket.EndAccept(asyncResult);
                SocketAccepted?.Invoke(s);
                _socket.BeginAccept(OnResult, null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Dispose()
        {
            _socket.Close();
            _socket.Dispose();
        }

        public delegate void SocketAcceptedHandler(Socket socket);
        public event SocketAcceptedHandler SocketAccepted;
    }
}
