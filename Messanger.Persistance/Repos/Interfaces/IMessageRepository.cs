﻿using Messanger.Persistance.Entities.Chat;
using Messanger.Persistance.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Persistance.Repos.Interfaces
{
    public interface IMessageRepository : IRepository<Message>
    {
    }
}
