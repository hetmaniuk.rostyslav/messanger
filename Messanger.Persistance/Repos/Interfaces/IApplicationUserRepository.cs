﻿using Messanger.Persistance.Entities;
using Messanger.Persistance.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Persistance.Repos.Interfaces
{
    public interface IApplicationUserRepository : IRepository<ApplicationUser>
    {
    }
}
