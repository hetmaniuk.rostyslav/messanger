﻿using Messanger.Persistance.Entities;
using Messanger.Persistance.Infrastructure;
using Messanger.Persistance.Repos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Persistance.Repos
{
    public class ApplicationUserRepository : Repository<ApplicationUser>, IApplicationUserRepository
    {
        public ApplicationUserRepository(MessangerDbContext dbContext) : base(dbContext)
        {
        }
    }
}
