﻿using Messanger.Persistance.Entities.Chat;
using Messanger.Persistance.Infrastructure;
using Messanger.Persistance.Repos.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Persistance.Repos
{
    public class MessageRepository : Repository<Message>, IMessageRepository
    {
        public MessageRepository(MessangerDbContext dbContext) : base(dbContext)
        {
        }
    }
}
