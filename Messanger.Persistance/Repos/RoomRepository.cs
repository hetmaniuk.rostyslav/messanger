﻿using Messanger.Persistance.Entities.Chat;
using Messanger.Persistance.Infrastructure;
using Messanger.Persistance.Repos.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Messanger.Persistance.Repos
{
    public class RoomRepository : Repository<Room>, IRoomRepository
    {
        public RoomRepository(MessangerDbContext dbContext) : base(dbContext)
        {
        }

        public new IEnumerable<Room> FindMany(Expression<Func<Room, bool>> predicate)
        {
            try
            {
                var dbSet = _dbContext.Set<Room>();
                return dbSet.Where(predicate).Include(x => x.ParticipantLinks).Include(x => x.Messages).ToList();
            }
            catch (Exception ex)
            {
                throw new DomainException("DbException FindMany " + typeof(Room).Name + ": " + ex.Message, ex);
            }
        }
    }
}
