﻿using Messanger.Persistance.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Messanger.Persistance.Infrastructure
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
    {
        protected readonly MessangerDbContext _dbContext;

        protected Repository(MessangerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        #region COUNT

        public int Count(Expression<Func<TEntity, bool>> predicate)
        {
            try
            {
                var dbSet = _dbContext.Set<TEntity>();
                return dbSet.Count(predicate);
            }
            catch (Exception ex)
            {
                throw new DomainException("DbException Count " + typeof(TEntity).Name + ": " + ex.Message, ex);
            }
        }

        #endregion

        #region DELETE

        public TEntity DeleteOne(Expression<Func<TEntity, bool>> predicate)
        {
            try
            {
                var dbSet = _dbContext.Set<TEntity>();
                var entity = dbSet.FirstOrDefault(predicate);

                if (entity == null)
                {
                    throw new DomainException("Exception in DeleteOne " + typeof(TEntity).Name);
                }

                _dbContext.Set<TEntity>().Remove(entity);

                _dbContext.SaveChanges();

                return entity;
            }
            catch (Exception ex)
            {
                throw new DomainException("DbException DeleteOne " + typeof(TEntity).Name + ": " + ex.Message, ex);
            }
        }

        public IEnumerable<TEntity> DeleteMany(Expression<Func<TEntity, bool>> predicate)
        {
            try
            {
                var dbSet = _dbContext.Set<TEntity>();
                var objects = dbSet.Where(predicate);

                foreach (var obj in objects)
                    dbSet.Remove(obj);

                _dbContext.SaveChanges();

                return objects;
            }
            catch (Exception ex)
            {
                throw new DomainException("DbException DeleteMany " + typeof(TEntity).Name + ": " + ex.Message, ex);
            }
        }

        public IEnumerable<TEntity> DeleteMany(IEnumerable<TEntity> entities)
        {
            try
            {
                var dbSet = _dbContext.Set<TEntity>();
                dbSet.RemoveRange(entities);

                _dbContext.SaveChanges();

                return entities;
            }
            catch (Exception ex)
            {
                throw new DomainException("DbException DeleteMany " + typeof(TEntity).Name + ": " + ex.Message, ex);
            }
        }

        #endregion

        #region FIND_MANY

        public IEnumerable<TEntity> FindMany(Expression<Func<TEntity, bool>> predicate)
        {
            try
            {
                var dbSet = _dbContext.Set<TEntity>();
                return dbSet.Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                throw new DomainException("DbException FindMany " + typeof(TEntity).Name + ": " + ex.Message, ex);
            }
        }

        #endregion

        #region FIND_ONE

        public TEntity FindOne(Expression<Func<TEntity, bool>> predicate)
        {
            try
            {
                var dbSet = _dbContext.Set<TEntity>();
                var result = dbSet.FirstOrDefault(predicate);

                return result;
            }
            catch (DomainException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DomainException("DbException FindOne " + typeof(TEntity).Name + ": " + ex.Message, ex);
            }
        }

        #endregion

        #region INSERT
        public IEnumerable<TEntity> InsertMany(IEnumerable<TEntity> entities)
        {
            try
            {
                _dbContext.Set<TEntity>().AddRange(entities);
                _dbContext.SaveChanges();

                return entities;
            }
            catch (Exception ex)
            {
                throw new DomainException("DbException InsertMany " + typeof(TEntity).Name + ": " + ex.Message, ex);
            }
        }

        public TEntity InsertOne(TEntity entity)
        {
            try
            {
                _dbContext.Set<TEntity>().Add(entity);
                _dbContext.SaveChanges();

                return entity;
            }
            catch (Exception ex)
            {
                throw new DomainException("DbException InsertOne " + typeof(TEntity).Name + ": " + ex.Message, ex);
            }
        }

        #endregion

        #region UPDATE
        public IEnumerable<TEntity> UpdateMany(IEnumerable<TEntity> entities)
        {
            try
            {
                _dbContext.Set<TEntity>().UpdateRange(entities);
                _dbContext.SaveChanges();

                return entities;
            }
            catch (Exception ex)
            {
                throw new DomainException("DbException UpdateMany " + typeof(TEntity).Name + ": " + ex.Message, ex);
            }
        }

        public IEnumerable<TEntity> UpdateMany(Expression<Func<TEntity, bool>> predicate, Action<TEntity> action)
        {
            try
            {
                var dbSet = _dbContext.Set<TEntity>();

                dbSet.ForEachAsync(action).GetAwaiter().GetResult();
                _dbContext.SaveChanges();

                return dbSet.Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                throw new DomainException("DbException UpdateMany " + typeof(TEntity).Name + ": " + ex.Message, ex);
            }
        }

        public TEntity UpdateOne(TEntity entity)
        {
            try
            {
                if (entity == null) throw new DomainException("Exception in UpdateOne " + typeof(TEntity).Name);

                _dbContext.Set<TEntity>().Update(entity);
                _dbContext.SaveChanges();

                return entity;
            }
            catch (DomainException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DomainException("DbException UpdateOne " + typeof(TEntity).Name + ": " + ex.Message, ex);
            }
        }

        #endregion
    }

}
