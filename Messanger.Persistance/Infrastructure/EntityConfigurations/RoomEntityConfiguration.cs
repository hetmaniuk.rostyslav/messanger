﻿using Messanger.Persistance.Entities.Chat;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Persistance.Infrastructure.EntityConfigurations
{
    public class RoomEntityConfiguration : IEntityTypeConfiguration<Room>
    {
        public void Configure(EntityTypeBuilder<Room> builder)
        {
            if (builder == null) return;

            builder
                .ToTable("rooms");

            builder
                .HasKey(o => o.Id);

            builder
                .Property<string>("Name")
                    .HasColumnName("name")
                    .IsRequired();

            builder
                .HasMany<Message>()
                .WithOne(x => x.Room)
                .HasForeignKey(x => x.RoomId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
