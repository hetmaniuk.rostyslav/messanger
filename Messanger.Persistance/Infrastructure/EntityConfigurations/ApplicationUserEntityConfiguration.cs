﻿using Messanger.Persistance.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Persistance.Infrastructure.EntityConfigurations
{
    public class ApplicationUserEntityConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            if (builder == null) return;

            builder
                .ToTable("application_users");

            builder
                .HasKey(o => o.Id);

            builder
                .Property<string>("Name")
                    .HasColumnName("name")
                    .IsRequired();

            builder
                .Property<string>("Email")
                    .HasColumnName("email")
                    .IsRequired();

            builder
                .Property<string>("Password")
                    .HasColumnName("password")
                    .IsRequired();
        }
    }
}
