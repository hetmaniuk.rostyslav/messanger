﻿using Messanger.Persistance.Entities;
using Messanger.Persistance.Entities.Chat;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Persistance.Infrastructure.EntityConfigurations
{
    public class MessageEntityConfiguration : IEntityTypeConfiguration<Message>
    {
        public void Configure(EntityTypeBuilder<Message> builder)
        {
            if (builder == null) return;

            builder
                .ToTable("message");

            builder
                .HasKey(o => o.Id);

            builder
                .Property<string>("Text")
                    .HasColumnName("text")
                    .IsRequired();

            builder
                .HasOne(x => x.Owner);

            builder
                .HasOne<Room>()
                .WithMany(x => x.Messages)
                .HasForeignKey(x => x.RoomId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
