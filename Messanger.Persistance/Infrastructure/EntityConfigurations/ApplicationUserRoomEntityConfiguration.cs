﻿using Messanger.Persistance.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Persistance.Infrastructure.EntityConfigurations
{
    public class ApplicationUserRoomEntityConfiguration : IEntityTypeConfiguration<ApplicationUserRoom>
    {
        public void Configure(EntityTypeBuilder<ApplicationUserRoom> builder)
        {
            if (builder == null) return;

            builder
                .HasKey(x => new { x.UserId, x.RoomId });

            //If you name your foreign keys correctly, then you don't need this.
            builder
                .HasOne(pt => pt.User)
                .WithMany(p => p.RoomLinks)
                .HasForeignKey(pt => pt.UserId);

            builder
                .HasOne(pt => pt.Room)
                .WithMany(t => t.ParticipantLinks)
                .HasForeignKey(pt => pt.RoomId);
        }
    }
}
