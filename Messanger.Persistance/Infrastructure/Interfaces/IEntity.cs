﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Persistance.Infrastructure.Interfaces
{
    public interface IEntity
    {
        string Id { get; set; }
        bool Validate();
    }
}
