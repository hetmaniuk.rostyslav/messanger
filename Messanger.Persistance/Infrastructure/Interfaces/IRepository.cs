﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Messanger.Persistance.Infrastructure.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class, IEntity
    {
        #region COUNT

        int Count(Expression<Func<TEntity, bool>> predicate);
        
        #endregion

        #region DELETE

        TEntity DeleteOne(Expression<Func<TEntity, bool>> predicate);
        IEnumerable<TEntity> DeleteMany(Expression<Func<TEntity, bool>> predicate);
        IEnumerable<TEntity> DeleteMany(IEnumerable<TEntity> entities);

        #endregion

        #region FIND_MANY
        
        IEnumerable<TEntity> FindMany(Expression<Func<TEntity, bool>> predicate);
        
        #endregion

        #region FIND_ONE
        
        TEntity FindOne(Expression<Func<TEntity, bool>> predicate);
        
        #endregion

        #region INSERT

        IEnumerable<TEntity> InsertMany(IEnumerable<TEntity> entities);

        TEntity InsertOne(TEntity entity);
        
        #endregion

        #region UPDATE
        
        TEntity UpdateOne(TEntity entity);
        IEnumerable<TEntity> UpdateMany(IEnumerable<TEntity> entities);
        IEnumerable<TEntity> UpdateMany(Expression<Func<TEntity, bool>> predicate, Action<TEntity> action);
        
        #endregion
    }

}
