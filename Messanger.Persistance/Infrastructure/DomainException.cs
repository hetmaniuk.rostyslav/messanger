﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Persistance.Infrastructure
{
    public class DomainException : Exception
    {
        public string Error { get; set; }
        public DomainException(string error)
        {
            this.Error = error;
        }

        public DomainException()
        {
        }

        public DomainException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
