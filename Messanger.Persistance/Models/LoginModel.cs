﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Persistance.Models
{
    public class LoginModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    
        public bool Validate()
        {
            return !string.IsNullOrWhiteSpace(Email) && 
                    !string.IsNullOrWhiteSpace(Password);
        }
    }
}
