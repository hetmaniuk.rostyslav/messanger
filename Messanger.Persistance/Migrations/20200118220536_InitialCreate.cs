﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Messanger.Persistance.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "application_users",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    name = table.Column<string>(nullable: false),
                    email = table.Column<string>(nullable: false),
                    password = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_application_users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "rooms",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_rooms", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ApplicationUserRoom",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoomId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationUserRoom", x => new { x.UserId, x.RoomId });
                    table.ForeignKey(
                        name: "FK_ApplicationUserRoom_rooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "rooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApplicationUserRoom_application_users_UserId",
                        column: x => x.UserId,
                        principalTable: "application_users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "message",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    text = table.Column<string>(nullable: false),
                    OwnerId = table.Column<string>(nullable: true),
                    RoomId = table.Column<string>(nullable: true),
                    RoomId1 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_message", x => x.Id);
                    table.ForeignKey(
                        name: "FK_message_application_users_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "application_users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_message_rooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "rooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_message_rooms_RoomId1",
                        column: x => x.RoomId1,
                        principalTable: "rooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationUserRoom_RoomId",
                table: "ApplicationUserRoom",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_message_OwnerId",
                table: "message",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_message_RoomId",
                table: "message",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_message_RoomId1",
                table: "message",
                column: "RoomId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApplicationUserRoom");

            migrationBuilder.DropTable(
                name: "message");

            migrationBuilder.DropTable(
                name: "application_users");

            migrationBuilder.DropTable(
                name: "rooms");
        }
    }
}
