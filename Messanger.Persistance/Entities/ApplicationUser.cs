﻿using Messanger.Persistance.Entities.Chat;
using Messanger.Persistance.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Messanger.Persistance.Entities
{
    public class ApplicationUser : IEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public List<ApplicationUserRoom> RoomLinks { get; set; }

        public bool Validate()
        {
            return !string.IsNullOrWhiteSpace(Name) && 
                    !string.IsNullOrWhiteSpace(Email) && 
                    !string.IsNullOrWhiteSpace(Password);
        }
    }
}
