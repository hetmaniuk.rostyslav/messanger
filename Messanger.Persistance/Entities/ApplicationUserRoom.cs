﻿using Messanger.Persistance.Entities.Chat;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messanger.Persistance.Entities
{
    public class ApplicationUserRoom
    {
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string RoomId { get; set; }
        public Room Room { get; set; }
    }
}
