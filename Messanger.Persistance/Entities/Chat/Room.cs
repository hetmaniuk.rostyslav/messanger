﻿using Messanger.Persistance.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Messanger.Persistance.Entities.Chat
{
    public class Room : IEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public string Name { get; set; }
        public List<ApplicationUserRoom> ParticipantLinks { get; set; }
        public List<Message> Messages { get; set; }

        public bool Validate()
        {
            return !string.IsNullOrWhiteSpace(Name);
        }
    }
}
