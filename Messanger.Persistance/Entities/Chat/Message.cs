﻿using Messanger.Persistance.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Messanger.Persistance.Entities.Chat
{
    public class Message : IEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public string Text { get; set; }
        public string OwnerId { get; set; }
        public ApplicationUser Owner { get; set; }
        public string RoomId { get; set; }
        public Room Room { get; set; }

        public bool Validate()
        {
            return !string.IsNullOrWhiteSpace(Text) && 
                    !string.IsNullOrWhiteSpace(OwnerId) && 
                    !string.IsNullOrWhiteSpace(RoomId);
        }
    }
}
