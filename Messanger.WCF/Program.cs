﻿using JKang.IpcServiceFramework;
using Messanger.WCF.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Messanger.WCF
{
    public class Program
    {
        static void Main(string[] args)
        {
            IServiceCollection services = ConfigureServices(new ServiceCollection());

            var host = new IpcServiceHostBuilder(services.BuildServiceProvider())
                .AddNamedPipeEndpoint<IMessangerCommunicationService>(name: "messageEndpoint", pipeName: "pipeName")
                .AddTcpEndpoint<IMessangerCommunicationService>(name: "messageTcpEndpoint", ipEndpoint: IPAddress.Loopback, port: 45684)
                .Build();

            var source = new CancellationTokenSource();

            Task.WaitAll(host.RunAsync(source.Token), Task.Run(() =>
            {
                Console.WriteLine("Press any key to shutdown.");
                Console.ReadKey();
                source.Cancel();
            }));

            Console.WriteLine("Server stopped.");
        }

        private static IServiceCollection ConfigureServices(IServiceCollection services)
        {
            return services
                .AddLogging(builder =>
                {
                    builder.SetMinimumLevel(LogLevel.Debug);
                })
                .AddIpc(builder =>
                {
                    builder
                        .AddNamedPipe(options =>
                        {
                            options.ThreadCount = 2;
                        })
                        .AddService<IMessangerCommunicationService, MessangerCommunicationService>();
                });
        }
    }
}
