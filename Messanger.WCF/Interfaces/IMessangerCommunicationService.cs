﻿using Messanger.Protocol.Messages.In;
using Messanger.Protocol.Messages.Out;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;

namespace Messanger.WCF.Interfaces
{
    /// <summary>
    /// The interface describes functionality provided by MyExchange server
    /// </summary>
    [ServiceContract(Name = "IMessangerComunicationService", CallbackContract = typeof(IClientCallback), SessionMode = SessionMode.Required)]
    public interface IMessangerCommunicationService
    {
        /// <summary>
        /// The method must be called first to authenticate account's credetials
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password">user's password</param>
        [OperationContract(IsOneWay = false, IsInitiating = true, IsTerminating = false)]
        [FaultContract(typeof(MessangerCommunicationException), Action = "http://uri/MessangerCommunicationException")]
        void Logon(string email, string password);

        /// <summary>
        /// logout and terminate session
        /// </summary>
        [OperationContract(IsOneWay = true, IsInitiating = false, IsTerminating = true)]
        void Logout();

        /// <summary>
        /// send async message
        /// </summary>
        /// <param name="messageIn">a message</param>
        [OperationContract(IsOneWay = true, IsInitiating = false, IsTerminating = false)]
        void Post(MessageIn messageIn);

    }


    /// <summary>
    /// The interface describes functionality to send an async response
    /// </summary>
    public interface IClientCallback
    {
        [OperationContract(IsOneWay = true)]
        void MessageReady(MessageOut messageOut);
    }

    /// <summary>
    /// Object of the class generates on server side to notify client about an error that occured processing a sync message
    /// </summary>
    [DataContract]
    public class MessangerCommunicationException
    {
        [DataMember(IsRequired = true)]
        public string Message;

        public MessangerCommunicationException(string msg)
        {
            this.Message = msg;
        }
    }


}
